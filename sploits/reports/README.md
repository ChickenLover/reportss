# Vulnerabilities

## SSRF (Server side request forgery)

Threre is insecure functionality of downloading arbitary files. It allows adversaries to trick the server to send GET request to the MongoDB REST API. In combination with no password hashing and no database authentication it leads to adversary becoming able to impersonate any user.

### Fix
Implement password hashing. Add database authentication. Blacklist database url

## SSTI (Server side template injection)

The 404 error page sends users some debug data like their own User-Agents. The data is being
processed in an insecure way, by inserting it in template text before it is being processed by a
template engine. It allows adversary to craft special User-Agent (`{{ config }}` for example), that
will lead to data leak. If the Flask app secret is leaked - adversary can then craft any session
and impersonate any user

### Fix
Pass User-Agent to the template engine as a variable.

## ZipLink

The service allows users to upload their reports in chunks (in ZIP archives). Backend decompress
them in an insecure way (by passing the `-X` flag to the `unzip` command). Adversary can than steal
any file from the server filesystem. A good choice will be `backup.zip` file in each user upload
folder, which contains all the files of users.

### Fix
Remove the `-X` flag
