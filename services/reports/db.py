import pymongo
import requests

from conf import DB_REST_HOST


DB_HTTP_URL = f'http://{DB_REST_HOST}'
USERS_COL = 'users'


def get_new_users():
    resp = requests.get(f'{DB_HTTP_URL}/{USERS_COL}/',
                params={'__limit': 50, '__sort': "-_id"}).json()
    return resp
    

def get_user(username):
    resp = requests.get(f'{DB_HTTP_URL}/{USERS_COL}/',
                        params={'username': username}).json()
    if resp:
        return resp[0]

def add_user(username, password):
    resp = requests.post(f'{DB_HTTP_URL}/{USERS_COL}/',
                         json={'username': username,
                               'password': password,
                               'reports': []}).json()

def add_report(username, filename):
    user = requests.get(f'{DB_HTTP_URL}/{USERS_COL}/',
                        params={'username': username}).json()[0]
    reports = user.get('reports', [])
    reports.append(filename)
    resp = requests.patch(f'{DB_HTTP_URL}/{USERS_COL}/{user["_id"]}',
            json={'reports': reports}).json()

def get_reports(username):
    user = requests.get(f'{DB_HTTP_URL}/{USERS_COL}/',
                        params={'username': username}).json()[0]
    if user:
        return user.get('reports', [])
    else:
        return []
