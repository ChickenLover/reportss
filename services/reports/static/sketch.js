
let r = 150;
let d = r * 2;
let yoff = 0.0;
let p1 = -r / 2;
let p2 = -p1;

function setup() {
  var cnv = createCanvas(windowWidth, windowHeight);
  // cnv.style('display', 'block');
  cnv.parent('sketch-holder');
  
  translate(width/2, height/2);
  p = createVector(r, 0);
  b = p.copy().rotate(QUARTER_PI - 0.4);
  a = p.copy().rotate(HALF_PI + QUARTER_PI + 0.4, OPEN);
  
  fr = createVector(r, 0);
  fl = createVector(-r, 0);
  ft = createVector(0, -r);
  fb = createVector(0, r);
}

function drawFlag() {
  strokeWeight(8);
  fill("#2F72FF");
  ellipse(0, 0, d + 4);
  fill(214, 212, 212);
  arc(0, 0, d + 4, d + 4, PI + QUARTER_PI - 0.4, PI + HALF_PI + QUARTER_PI + 0.4, OPEN)
  fill("#B60016");
  arc(0, 0, d, d, QUARTER_PI - 0.4, HALF_PI + QUARTER_PI + 0.4, OPEN)
}

function drawBlood() {
  fill("#B60016");
  strokeWeight(0);
  beginShape();
  let xoff = 0;

  for (let x = -r; x <= r; x += 10) {
    let yd = 50
    let y = map(noise(xoff, yoff), 0, 1, a.y - yd, a.y);

    if (sqrt(r * r - x * x) < y) {
      continue
    }
    
    vertex(x, y);
    xoff += 0.05;
  }
  yoff += 0.01;
  
  for (let i = 0.2; i < PI - 0.1; i += 0.2) {
    let x = cos(i)
    let y = sin(i)
    vertex(x * r, y * r)
  }
  endShape(CLOSE);
}

function drawZ() {
  push()
  //rotate(-0.1)
  stroke(0, 255);
  strokeWeight(16);
  line(p1, p1, p2, p1);
  line(p2, p1, p1, p2);
  line(p1, p2, p2, p2);
  pop()
}

function drawOverCircle() {
  noFill();
  stroke(0, 255);
  strokeWeight(4);
  ellipse(0, 0, d);
  strokeWeight(16);
  for (let i = 1; i < 10; i +=1) {
    stroke("#c90000");
    ellipse(0, 0, d + 4 + 16 * i);
  }  
}

function circlePoint(x) {
  return sqrt(r * r / 4 - x * x)
}

function drawCross() {
  noStroke()
  fill(255)
  rectMode(CENTER)
  rect(0, 0, width, r)
  fill(0)
  rect(0, 0, width, r / 2)
  strokeWeight(10)
  stroke(0)
  line(-width / 2, -r/2 + r/10, width / 2, -r/2 + r/10)
  line(-width / 2, r/2 - r/10, width / 2, r/2 - r/10)
}

function draw() {
  resizeCanvas(windowWidth, windowHeight);
  translate(width/3, height/2);
  background("#92a8d1");
  drawCross()
  rotate(HALF_PI)
  drawCross()
  rotate(-HALF_PI)
  translate(width / 2, 0);
  drawCross()
  translate(-(width / 2), 0);
  
  drawFlag()
  drawBlood()
  //drawOverCircle()
  drawZ()
  
  // x^2 + y^2 = r^2
  // y = sqrt(r^2 - x^2)
  
  strokeWeight(4)
  stroke("black")
  strokeWeight(8)  
}
