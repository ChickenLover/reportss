#!/usr/bin/env python3

import re
import os
import uuid
import base64
import random
import zipfile
import subprocess

import requests
from flask import Flask, request, render_template, flash, redirect, url_for,\
                  render_template_string, session, send_from_directory

from bhash import bhash
from db import add_user, get_user, get_reports, add_report, get_new_users

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = 'reports'
SECRET_SEED = 1337 # Don't change to random - gunicorn multiple workers would crash
app.secret_key = bhash("Z", SECRET_SEED)

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/change-browser')
def change_browser():
    return render_template("change-browser.html")

# ================= AUTH ==================

@app.route('/login', methods=['GET', 'POST'])
def login():
    msg = ''
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
        username = request.form['username']
        password = request.form['password']

        account = get_user(username)
        if account and account.get("password") == password:
            session['loggedin'] = True
            session['username'] = account['username']
            msg = 'Вы успешо авторизованы в системе!'
        else:
            msg = 'Не удалось войти с вашими данными'
    return render_template('login.html', msg = msg)

@app.route('/logout')
def logout():
    session.pop('loggedin', None)
    session.pop('username', None)
    return redirect('/login')

@app.route('/register', methods=['GET', 'POST'])
def register():
    msg = ''
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
        username = request.form['username']
        password = request.form['password']

        username = re.sub('[^\d\w\-]', '', username)
        if not username:
            msg = 'Некорректное имя пользователя! Разрешены только цифры и буквы'
        else:
            account = get_user(username)
            if account:
                msg = 'Такой аккаунт уже есть!'
            elif not username or not password:
                msg = 'Заполните форму корректно!'
            else:
                add_user(username, password)
                msg = 'Поздравляю, вы зарегистрированый! Теперь, войдите в свой аккаунт'
    elif request.method == 'POST':
        msg = 'Заполните форму корректно!'
    return render_template('register.html', msg = msg)

# ========================================

@app.route('/users')
def users():
    users = [x['username'] for x in get_new_users()]
    return render_template("users.html", users=users)


def get_user_folder(session):
    return os.path.join(app.config['UPLOAD_FOLDER'], session['username'])


def add_to_backup(filepath):
    user_folder = os.path.dirname(filepath)
    filename = os.path.basename(filepath)
    backup_path = os.path.join(user_folder, "backup.zip")
    with zipfile.ZipFile(backup_path, "a") as z:
        with open(filepath, 'rb') as new_file:
            z.writestr(filename, new_file.read())


def save_user_file(u_file, session):
    user_folder = get_user_folder(session)
    if not os.path.exists(user_folder):
        os.mkdir(user_folder)
    filename = u_file.filename.replace('/', '').replace(' ', '_')
    filepath = os.path.join(user_folder, filename)
    u_file.save(filepath)
    add_to_backup(filepath)
    return filename


def process_zip(zipname, session):
    user_folder = get_user_folder(session)
    zippath = os.path.join(user_folder, zipname)
    subprocess.call(["unzip", "-qqoX", zippath, "-d", user_folder])
    with zipfile.ZipFile(zippath, 'r') as zip_ref:
        for archive_file in zip_ref.namelist():
            add_report(session['username'], archive_file)


@app.route('/lk', methods=['GET', 'POST'])
def lk():
    msg = ''
    if not session.get('loggedin'):
        return redirect(url_for('login'))
    if request.method == 'POST' and session.get('loggedin'):
        if 'file' not in request.files or not request.files['file'] or not request.files['file'].filename:
            msg = 'Ошибка!'
        else:
            u_file = request.files['file']
            filename = save_user_file(u_file, session)
            if filename.endswith('.zip'):
                process_zip(filename, session)
            else:
                add_report(session['username'], filename)
            msg = 'Файл загружен!'
    reports = get_reports(session['username'])
    reports = [{ 
                 'name': fname,
                 'path': f'/reports/{fname}'
               } for fname in reports]
    return render_template('lk.html', msg=msg, reports=reports)

@app.route('/upload_url', methods=['POST'])
def upload():
    if not session or not session.get('loggedin'):
        return redirect(url_for('login'))

    URL_REGEX = "^((http[s]?|ftp):\/\/)?\/?([^\/\.]+\.)*?([^\/\.]+\.?[^:\/\s\.]{1,3}(\.[^:\/\s\.]{2,3})?(:\d+)?)($|\/)([^#?\s]+)?(.*?)?(#[\w\-]+)?$"
    msg = ''
    if 'url' in request.form:
        url = request.form['url']
        if not re.match(URL_REGEX, url):
            msg = "Invalid url!"
        else:
            resp = requests.get(url)
            parts = re.search(URL_REGEX, url)
            filename = parts[4] + '_' + str(uuid.uuid4())
            user_folder = os.path.join(app.config['UPLOAD_FOLDER'], session['username'])
            if not os.path.exists(user_folder):
                os.mkdir(user_folder)
            with open(os.path.join(user_folder, filename), 'wb') as f:
                f.write(resp.content)
            add_report(session['username'], filename)
    return redirect(url_for('lk'))

@app.route('/reports/<path:filename>', methods=['GET', 'POST'])
def download(filename):
    filename = filename.replace('/', '')
    if not session or not session.get('loggedin'):
        return redirect(url_for('login'))
    uploads = os.path.join(app.config['UPLOAD_FOLDER'], session['username'])
    return send_from_directory(uploads, filename)

@app.route('/backup', methods=['GET'])
def download_backup():
    if not session or not session.get('loggedin'):
        return redirect(url_for('login'))
    uploads = os.path.join(app.config['UPLOAD_FOLDER'], session['username'])
    return send_from_directory(uploads, "backup.zip")

@app.route('/error_404')
def error():
    response = '''
        {{% extends "base.html" %}}
        {{% block content %}}
        <b>Министерство</b> приносит свои искренние извинения, но вы попали на несуществующую страницу!<br>
        Если вы считаете что произошла ошибка - обратитесь в тех-поддержку<br>

        Информация для отладки:<br>
        <ul>
            <li>Версия браузера: {}</li>
            <li>Ваш IP адрес: {}</li>
        </ul>
        {{% endblock %}}
    '''
    return render_template_string(response.format(request.headers.get('User-Agent'), request.remote_addr))

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=4000)
