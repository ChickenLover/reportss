Flask==2.0.0
requests==2.27.1
pymongo==4.1.0
gunicorn==20.1.0
numpy==1.18.0
