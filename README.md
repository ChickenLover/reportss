# ZPD-training-service 
Service for A/D CTF training written in python.

## reports

Service for cloud file storing

### Tags

- python
- flask
- jinja2
- mongodb
- web

### Vulnerabilities

- Server side template injection via custom User-Agent header. Triggered on the `/error_404` page [Sploit.](./sploits/reports/ssti.py)
- Server side request forgery via arbitrary GET requests functionallity. Can be exploited by queriing the HTTP API for mongodb, accesable only from the app container  [Sploit.](./sploits/reports/ssrf.py)
- Insecure ZIP extraction. Can be exploited by uploading a ZIP archive containing unix links to users backup.zip files [EvilZip](./sploits/reports/evil.zip) [Sploit.](./sploits/reports/ziplink.py)

More details [here](./sploits/reports/README.md)
## Deploy

### Service

```bash
cd ./services/reports
docker-compose up -d --build
```

### Checker

The checker interface matches the description for ructf: `https://github.com/HackerDom/ructf-2017/wiki/Интерфейс-«проверяющая-система-чекеры»`

```bash
cd ./checkers/reports
python3 checker.py 
```

To use it with ructf jury, you need to change the output format of the checker `info` function:
- comment this row https://gitlab.com/ChickenLover/reports/blob/master/checkers/fortuneteller/checker.py#L457
- delete comment from this row https://gitlab.com/ChickenLover/reports/blob/master/checkers/fortuneteller/checker.py#L458


## Contributors

@ChickenLover
