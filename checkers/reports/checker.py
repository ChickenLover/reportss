#!/usr/bin/env python3

import os
import io
import re
import sys
import json
import uuid
import time
import random
import string
import inspect
import zipfile
import datetime
from sys import argv
from enum import Enum

import requests

# Make all random more random.
random = random.SystemRandom()

""" <config> """
# SERVICE INFO
PORT = 8000
URLS = [
    "https://glavcom.ua/img/article/5503/97_main.jpg",
    "http://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2021-03/2021-02-22T000000Z_91076483_RC2WXL94TYUL_RTRMADP_3_GLOBAL-MARKETS%204.jpg?itok=7omH_WTk",
    "https://media.online47.ru/media/photo/article/__72594.jpg",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT5mIir8WQfIRy-SftlmC16XAEAUJr4JDCAu9GiQr5mps8ULKuJ0S7IDA5mBsy_l5OV09M&usqp=CAU",
    "https://ridus.ru/images/2022/3/31/1434428/in_article_webp_a97addd8b9.webp",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Report_On_The_Investigation_Into_Russian_Interference_In_The_2016_Presidential_Election.pdf/page1-1200px-Report_On_The_Investigation_Into_Russian_Interference_In_The_2016_Presidential_Election.pdf.jpg",
    "https://img.pravda.com/images/doc/f/b/fbed20f-fo7-mpvxsayqqvs.png",
    "https://cs14.pikabu.ru/post_img/2022/03/28/8/1648474047188773870.jpg",
    "https://storage1.censor.net/images/4/3/8/8/43880f60faf445684cbcf43b876bf091/640x897.jpg"
]

# DEBUG -- logs to stderr, TRACE -- log HTTP requests
DEBUG = os.getenv("DEBUG", True)
TRACE = os.getenv("TRACE", False)
""" </config> """
NAMES = open(os.path.join(os.path.abspath(os.path.dirname(__file__)), "names.txt")).read().split()
SURNAMES = open(os.path.join(os.path.abspath(os.path.dirname(__file__)), "surnames.txt")).read().split()
SOME_BOOK = open(os.path.join(os.path.abspath(os.path.dirname(__file__)), "some_book.txt")).read().split('\n')
IMAGES = [os.path.join(os.path.abspath(os.path.dirname(__file__)), f"img{i}.png") for i in range(1, 7)]

# check: put -> get
# check: all users are displayed 
def check(host: str):
    s = FakeSession(host, PORT)
    name = _gen_secret_name()
    password = rand_string()
    report_name = _gen_random_report_name()
    report_data = _gen_random_report()

    _register(s, name, password)
    _login(s, name, password)
    _log(f"Going to save report '{name}'")
    report_link = _put(s, report_name, report_data)
    if report_data not in _get(s, report_link):
        die(ExitStatus.MUMBLE, "Can't find uploaded report")

    _log("Check all users are displayed")
    if not _check_users(s, name, host):
        die(ExitStatus.MUMBLE, "failed to check users ")

    _log("Check image upload")
    if not _check_image_upload(s):
        die(ExitStatus.MUMBLE, "failed to check image upload ")

    # _log("Check url upload")
    # if not _check_upload_by_url(s):
    #    die(ExitStatus.MUMBLE, "failed to check url upload ")

    _log("Check zip upload")
    if not _check_zip_upload(s):
        die(ExitStatus.MUMBLE, "failed to check zip upload")

    _log("Check download all")
    if not _check_download_all(s):
        die(ExitStatus.MUMBLE, "failed to download all")

    _log("Check 404 error page")
    if not _check_error(s):
        die(ExitStatus.MUMBLE, "failed to check 404 error page")

    die(ExitStatus.OK, "Check ALL OK")


def put(host: str, flag_id: str, flag: str):
    s = FakeSession(host, PORT)
    username = _gen_secret_name()
    password = rand_string()
    _register(s, username, password)
    _login(s, username, password)

    flag_link = _put(s, flag_id, flag)

    jd = json.dumps({
        "flag_id": flag_link,
        "username": username,
        "password": password,
    })

    print(jd, flush=True)  # It's our flag_id now! Tell it to jury!
    die(ExitStatus.OK, f"{jd}")


def get(host: str, flag_id: str, flag: str):
    try:
        data = json.loads(flag_id)
        if not data:
            raise ValueError
    except:
        die(
            ExitStatus.CHECKER_ERROR,
            f"Unexpected flagID from jury: {flag_id}! Are u using non-RuCTF checksystem?",
        )

    s = FakeSession(host, PORT)
    _login(s, data["username"], data["password"])

    _log("Getting flag from lk")
    flag_data = _get(s, data["flag_id"])
    if flag not in flag_data:
        die(ExitStatus.CORRUPT, f"Can't find a flag in {flag_data}")
    die(ExitStatus.OK, f"All OK! Successfully retrieved a flag from api")


class FakeSession(requests.Session):
    """
    FakeSession reference:
        - `s = FakeSession(host, PORT)` -- creation
        - `s` mimics all standard request.Session API except of fe features:
            -- `url` can be started from "/path" and will be expanded to "http://{host}:{PORT}/path"
            -- for non-HTTP scheme use "https://{host}/path" template which will be expanded in the same manner
            -- `s` uses random browser-like User-Agents for every requests
            -- `s` closes connection after every request, so exploit get splitted among multiple TCP sessions
    Short requests reference:
        - `s.post(url, data={"arg": "value"})`          -- send request argument
        - `s.post(url, headers={"X-Boroda": "DA!"})`    -- send additional headers
        - `s.post(url, auth=(login, password)`          -- send basic http auth
        - `s.post(url, timeout=1.1)`                    -- send timeouted request
        - `s.request("CAT", url, data={"eat":"mice"})`  -- send custom-verb request
        (response data)
        - `r.text`/`r.json()`  -- text data // parsed json object
    """

    USER_AGENTS = [
        """Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1 Safari/605.1.15""",
        """Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36""",
        """Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201""",
        """Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.13; ) Gecko/20101203""",
        """Mozilla/5.0 (Windows NT 5.1) Gecko/20100101 Firefox/14.0 Opera/12.0""",
    ]

    def __init__(self, host, port):
        super(FakeSession, self).__init__()
        if port:
            self.host_port = "{}:{}".format(host, port)
        else:
            self.host_port = host

    def prepare_request(self, request):
        r = super(FakeSession, self).prepare_request(request)
        r.headers["User-Agent"] = random.choice(FakeSession.USER_AGENTS)
        r.headers["Connection"] = "Keep-Alive"
        return r

    # fmt: off
    def request(self, method, url,
                params=None, data=None, headers=None,
                cookies=None, files=None, auth=None, timeout=None, allow_redirects=True,
                proxies=None, hooks=None, stream=None, verify=None, cert=None, json=None,
                ):
        if url[0] == "/" and url[1] != "/":
            url = "http://" + self.host_port + url
        else:
            url = url.format(host=self.host_port)
        r = super(FakeSession, self).request(
            method, url, params, data, headers, cookies, files, auth, timeout,
            allow_redirects, proxies, hooks, stream, verify, cert, json,
        )
        if TRACE:
            print("[TRACE] {method} {url} {r.status_code}".format(**locals()))
        return r
    # fmt: on


def _register(s, name, password):
    try:
        r = s.post("/register", data={
                        "username": name,
                        "password": password,
                    })
    except Exception as e:
        die(ExitStatus.DOWN, f"Failed to register in service: {e}")

    if r.status_code != 200:
        die(ExitStatus.MUMBLE, f"Unexpected /register code {r.status_code}")


def _login(s, name, password):
    try:
        r = s.post("/login", data={
                        "username": name,
                        "password": password,
                    })
    except Exception as e:
        die(ExitStatus.DOWN, f"Failed to login in service: {e}")

    if r.status_code != 200:
        die(ExitStatus.MUMBLE, f"Unexpected /login code {r.status_code}")

def _put(s, filename, flag):
    try:
        flag_data = io.BytesIO(flag.encode())
        r = s.post("/lk", files={"file": (filename, flag_data)})
    except Exception as e:
        die(ExitStatus.DOWN, f"Failed to send flag upload request: {e}")

    if r.status_code != 200:
        die(ExitStatus.MUMBLE, f"Unexpected POST /lk code {r.status_code}")

    if not "Файл загружен" in r.text:
        die(ExitStatus.MUMBLE, "No 'Файл загружен' in upload response")

    files_links = re.findall('(\/reports\/.*?)>', r.text)
    flag_link = f"/reports/{filename}"
    if not flag_link in files_links:
        die(ExitStatus.MUMBLE, "No flag link in lk after flag upload")
    return flag_link


def _get(s, flag_link):
    try:
        r = s.get("/lk")
    except Exception as e:
        die(ExitStatus.DOWN, f"Failed to get lk page: {e}")

    if r.status_code != 200:
        die(ExitStatus.MUMBLE, f"Unexpected GET /lk code {r.status_code}")

    files_links = re.findall('(\/reports\/.*?)>', r.text)
    if not flag_link in files_links:
        die(ExitStatus.CORRUPT, "No flag link in lk")

    try:
        r = s.get(flag_link)
    except Exception as e:
        die(ExitStatus.DOWN, f"Failed to get lk page: {e}")

    return r.content.decode()


def _check_users(s, name, host):
    s_second = FakeSession(host, PORT)
    try:
        r = s_second.get("/users")
    except Exception as e:
        die(ExitStatus.DOWN, f"Failed to get all users: {e}")

    if r.status_code != 200:
        die(ExitStatus.MUMBLE, f"Unexpected  /users code {r.status_code}")

    users = re.findall('<b>(.*?)<\/b>', r.text)
    if name not in users:
        _log(f"Cant find this user {name} in {users}")
        return False
    return True


def _check_image_upload(s):
    img = random.choice(IMAGES)
    img_name = os.path.basename(img)
    try:
        with open(img, 'rb') as f:
            r = s.post("/lk", files={"file": f})
    except Exception as e:
        die(ExitStatus.DOWN, f"Failed to send image upload request: {e}")

    if r.status_code != 200:
        die(ExitStatus.MUMBLE, f"Unexpected POST /lk code {r.status_code}")

    if not "Файл загружен" in r.text:
        die(ExitStatus.MUMBLE, "No 'Файл загружен' in upload response")

    files_links = re.findall('(\/reports\/.*?)>', r.text)
    if not files_links:
        die(ExitStatus.MUMBLE, "No files in lk after flag upload")

    if not any(img_name in link for link in files_links):
        _log(f"Cant find img link {img_name} in {files_links}")
        return False
    return True


def _check_upload_by_url(s):
    url = random.choice(URLS)
    check_word = '.'.join(url.split('/')[2].split('.')[-2:])
    try:
        r = s.post("/upload_url", data={"url": url})
    except Exception as e:
        pass

    try:
        r = s.get("/lk")
    except Exception as e:
        die(ExitStatus.DOWN, f"Failed to send lk request: {e}")

    files_links = re.findall('(\/reports\/.*?)>', r.text)
    if not files_links:
        die(ExitStatus.MUMBLE, "No files in lk after upload by url")

    if not any(check_word in link for link in files_links):
        _log(f"Cant find img link {img_name} in {files_links}")
        return False
    return True


def _check_zip_upload(s):
    arch_path = f"/tmp/{uuid.uuid4()}.zip"
    files = []
    try:
        with zipfile.ZipFile(arch_path, "w") as z:
            for i in range(random.randint(1, 4)):
                fname = f"report{i+1}.txt"
                data = _gen_random_report()
                files.append((fname, data))
                z.writestr(fname, data.encode())
        try:
            with open(arch_path, 'rb') as f:
                r = s.post("/lk", files={"file": f})
        except Exception as e:
            die(ExitStatus.DOWN, f"Failed to send zip upload request: {e}")

        if r.status_code != 200:
            die(ExitStatus.MUMBLE, f"Unexpected POST /lk code {r.status_code}")

        if not "Файл загружен" in r.text:
            die(ExitStatus.MUMBLE, "No 'Файл загружен' in upload zip response")

        files_links = re.findall('(\/reports\/.*?)>', r.text)
        for fname, data in files:
            file_link = f"/reports/{fname}"
            if file_link not in files_links:
                _log(f"Cant find zip report link {file_link} in {files_links}")
                return False
            try:
                r = s.get(file_link)
            except Exception as e:
                die(ExitStatus.DOWN, f"Failed to download report from zip: {e}")
            if r.content != data.encode():
                _log(f"Invalid content in zip file! {r.content.encode()} found, {data} expected")
                return False
    except Exception as e:
        _log(f"Checker failed in _check_zip_upload: {e}")
        return False
    finally:
        if os.path.exists(arch_path):
            os.remove(arch_path)
    return True


def _check_download_all(s):
    files = []
    for i in range(random.randint(10, 14)):
        fname = f"report{i+1}.txt"
        data = _gen_random_report()
        files.append((fname, data))
        _put(s, fname, data)
    try:
        r = s.get("/backup")
    except Exception as e:
        die(ExitStatus.DOWN, f"Failed to download backup: {e}")
 
    if r.status_code != 200:
        die(ExitStatus.MUMBLE, f"Unexpected GET /backup code {r.status_code}")

    try:
        arch_path = f"/tmp/{uuid.uuid4()}.zip"
        with open(arch_path, "wb") as f:
            f.write(r.content)

        with zipfile.ZipFile(arch_path) as z:
            for fname, data in files:
                if fname not in z.namelist():
                    _log(f"Cant find {fname} in backup! Got {z.namelist()}")
                    return False
                arch_data = z.open(fname).read()
                if data.encode() != arch_data:
                    _log(f"Invalid content in zip file! {arch_data} found, {data} expected")
                    return False
    except Exception as e:
        _log(f"Checker failed in _check_download_all: {e}")
        return False
    finally:
        if os.path.exists(arch_path):
            os.remove(arch_path)
    return True


def _check_error(s):
    routes = [
        "/lk",
        "/login",
        "/register",
        "/users",
    ]
    route = random.choice(routes) + random.choice(string.ascii_lowercase)
    try:
        r = s.get(route)
    except Exception as e:
        die(ExitStatus.DOWN, f"Failed to send error request: {e}")
 
    if r.status_code != 404:
        die(ExitStatus.MUMBLE, f"Unexpected POST {route} code {r.status_code}")

    if r.request.headers["User-Agent"] not in r.text:
        _log(f"Cant find User-Agent on error page")
        return False
    return True


def _rand_name() -> str:
    name = random.choice(NAMES).capitalize()
    surname = random.choice(SURNAMES).capitalize()
    return name + " " + surname


def _gen_secret_name() -> str:
    return f"{_rand_name().replace(' ', '_')}_{random.randint(1939, 1999)}"


def _gen_random_report() -> str:
    titles = [
        f"За последние 24ч рубль укрепился на {random.randint(1, 2)}р. {random.randint(1, 100)}к.",
        f"Обвал доллара: западная валюта подешевела на {random.randint(1, 2)}р. {random.randint(1, 100)}к.",
        f"Система здравоохранения Р. зарегистрировала рекордно низкое кол-во болеющих коронавирусом {random.randint(125, 500)} т. ч.",
        f"Министр финансов - {_rand_name()}: \"Фондовый рынок Р. сейчас полностью стабилен. Торги Р. акциями будут открыты уже через {random.randint(60, 90)} дней\"",
    ]
    title = random.choice(titles)
    random.shuffle(SOME_BOOK)
    text = title + '\n\n\n' + '\n'.join(SOME_BOOK)
    return text

def _gen_random_report_name() -> str:
    return str(uuid.uuid4()) + ".txt"


def rand_string(n=12, alphabet=string.ascii_uppercase + string.ascii_lowercase + string.digits):
    return ''.join(random.choice(alphabet) for _ in range(n))


def _log(obj):
    if DEBUG and obj:
        caller = inspect.stack()[1].function
        print(f"[{caller}] {obj}", file=sys.stderr)
    return obj


class ExitStatus(Enum):
    OK = 101
    CORRUPT = 102
    MUMBLE = 103
    DOWN = 104
    CHECKER_ERROR = 110


def die(code: ExitStatus, msg: str):
    if msg:
        print(msg, file=sys.stderr)
    exit(code.value)


def info():
    print('{"vulns": 1, "timeout": 30, "attack_data": ""}', flush=True, end="")
    # print("vulns: 1", flush=True, end="")
    exit(101)


def _main():
    try:
        cmd = argv[1]
        hostname = argv[2]
        if cmd == "get":
            fid, flag = argv[3], argv[4]
            get(hostname, fid, flag)
        elif cmd == "put":
            fid, flag = argv[3], argv[4]
            put(hostname, fid, flag)
        elif cmd == "check":
            check(hostname)
        elif cmd == "info":
            info()
        else:
            raise IndexError
    except IndexError:
        die(
            ExitStatus.CHECKER_ERROR,
            f"Usage: {argv[0]} check|put|get IP FLAGID FLAG",
        )


if __name__ == "__main__":
    _main()
